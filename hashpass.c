#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    // Check 2 file names are given
    if (argc < 3)
    {
        fprintf(stderr, "You must supply 2 file names.\n");
        exit(1);
    }
    
    // Open the file for reading
    FILE *in;
    in = fopen(argv[1], "r");
    
    if (!in)
    {
        fprintf(stderr, "Can't open %s for reading.\n", argv[1]);
        exit(2);
    }
    
    // Open the file for writing & appending
    FILE *out;
    out = fopen(argv[2], "wa");
    
    if (!out)
    {
        fprintf(stderr, "Can't open %s for writing.\n", argv[2]);
        exit(3);
    }
    
    char linein[1000];
    
    while (fgets(linein, 1000, in) != NULL)
    {
        char *lineout = md5(linein, strlen(linein)-1);
        fprintf(out, "%s\n", lineout);
        free(lineout);
    }
    fclose(in);
    fclose(out);
}